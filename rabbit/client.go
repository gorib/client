package rabbit

import (
	"context"
	"encoding/json"
	"fmt"

	amqp "github.com/rabbitmq/amqp091-go"

	"gitlab.com/gorib/pry"
	"gitlab.com/gorib/session"
)

type Document struct {
	Id       string
	Document any
}

func WithLogger(logger pry.Logger) option {
	return func(t *transport) {
		t.logger = logger
	}
}

type option func(t *transport)

func New(dsn, exchange string, opts ...option) *transport {
	t := &transport{
		dsn:      dsn,
		exchange: exchange,
	}
	for _, opt := range opts {
		opt(t)
	}
	return t
}

type transport struct {
	dsn      string
	exchange string
	logger   pry.Logger
}

func (r *transport) Publish(ctx context.Context, documents ...*Document) error {
	connection, err := amqp.Dial(r.dsn)
	if err != nil {
		return fmt.Errorf("rabbit conn: %v", err)
	}
	defer func() {
		if err := connection.Close(); err != nil {
			if r.logger != nil {
				r.logger.Error(err, pry.Ctx(ctx))
			}
		}
	}()

	channel, err := connection.Channel()
	if err != nil {
		return fmt.Errorf("rabbit channel: %v", err)
	}
	defer func() {
		if err := channel.Close(); err != nil {
			if r.logger != nil {
				r.logger.Error(err, pry.Ctx(ctx))
			}
		}
	}()

	err = channel.ExchangeDeclare(r.exchange, "topic", true, false, false, false, nil)
	if err != nil {
		return fmt.Errorf("rabbit exchange: %v", err)
	}

	for _, document := range documents {
		payload, err := json.Marshal(document.Document)
		if err != nil {
			return fmt.Errorf("rabbit payload: %v", err)
		}
		message := amqp.Publishing{
			ContentType: "text/plain",
			Body:        payload,
			Headers:     map[string]any{},
		}
		if s := session.FromContext(ctx); s != nil {
			if s.CorrelationId != "" {
				message.CorrelationId = s.CorrelationId
			}
			if s.UserAgent != "" {
				message.Headers[session.AmqpUserAgentHeader] = s.UserAgent
			}
			if s.UserLanguage != "" {
				message.Headers[session.AmqpUserLanguageHeader] = s.UserLanguage
			}
			if s.SourceIp != "" {
				message.Headers[session.AmqpSourceIpHeader] = s.SourceIp
			}
		}

		err = channel.PublishWithContext(ctx, r.exchange, document.Id, false, false, message)
		if err != nil {
			return fmt.Errorf("rabbit publish: %v", err)
		}
	}
	return nil
}
