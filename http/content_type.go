package http

import "slices"

const (
	ContentTypeJson = "application/json"
	ContentTypeXml  = "application/xml"
	ContentTypeForm = "application/x-www-form-urlencoded"
)

type ContentType string

func (t ContentType) IsValid() bool {
	return slices.Contains([]ContentType{
		ContentTypeJson,
		ContentTypeXml,
		ContentTypeForm,
	}, t)
}
