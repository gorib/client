package http

import "net/http"

type Error struct {
	Message  string
	Response *http.Response
}

func (e *Error) Error() string {
	return e.Message
}

func Exception(message string, response *http.Response) *Error {
	return &Error{
		Message:  message,
		Response: response,
	}
}
