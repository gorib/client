package http

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
)

func Body(body *io.ReadCloser) ([]byte, error) {
	if body == nil || *body == nil || *body == http.NoBody {
		return nil, nil
	}
	data, err := io.ReadAll(*body)
	if err != nil {
		return nil, fmt.Errorf("body read: %v", err)
	}
	err = (*body).Close()
	if err != nil {
		return nil, fmt.Errorf("body close: %v", err)
	}
	buf := bytes.NewBuffer(data)
	*body = io.NopCloser(buf)
	return data, nil
}
