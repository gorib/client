package http

import (
	"bytes"
	"context"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"
)

type RequestOption func(r *http.Request) error

func WithJsonBody(body any) RequestOption {
	data, err := json.Marshal(body)
	if err != nil {
		return func(r *http.Request) error {
			return fmt.Errorf("json payload: %v", err)
		}
	}
	return withBody(bytes.NewBuffer(data), ContentTypeJson)
}

func WithXmlBody(body any) RequestOption {
	data, err := xml.Marshal(body)
	if err != nil {
		return func(r *http.Request) error {
			return fmt.Errorf("xml payload: %v", err)
		}
	}
	return withBody(bytes.NewBuffer(data), ContentTypeXml)
}

func WithUrlEncodedBody(body url.Values) RequestOption {
	return withBody(strings.NewReader(body.Encode()), ContentTypeForm)
}

func WithBasicAuth(username string, password string) RequestOption {
	return func(r *http.Request) error {
		r.SetBasicAuth(username, password)
		return nil
	}
}

func WithBasicAuthFunc(auth func(ctx context.Context) (string, string)) RequestOption {
	return func(r *http.Request) error {
		username, password := auth(r.Context())
		r.SetBasicAuth(username, password)
		return nil
	}
}

func WithBearerAuth(token string) RequestOption {
	return func(r *http.Request) error {
		r.Header.Set("Authorization", "Bearer "+token)
		return nil
	}
}

func WithBearerAuthFunc(token func(ctx context.Context) string) RequestOption {
	return func(r *http.Request) error {
		r.Header.Set("Authorization", "Bearer "+token(r.Context()))
		return nil
	}
}

func WithHeader(header string, values ...string) RequestOption {
	return func(r *http.Request) error {
		for _, value := range values {
			r.Header.Add(header, value)
		}
		return nil
	}
}

func WithHeaderFunc(header string, values func(ctx context.Context) <-chan string) RequestOption {
	return func(r *http.Request) error {
		for value := range values(r.Context()) {
			r.Header.Add(header, value)
		}
		return nil
	}
}

func withBody(body io.Reader, contentType string) RequestOption {
	// almost copy from net/http
	return func(r *http.Request) error {
		r.Header.Del("Content-Type")
		if body != nil {
			rc, ok := body.(io.ReadCloser)
			if !ok {
				rc = io.NopCloser(body)
			}
			r.Body = rc
			switch v := body.(type) {
			case *bytes.Buffer:
				r.ContentLength = int64(v.Len())
				buf := v.Bytes()
				r.GetBody = func() (io.ReadCloser, error) {
					r := bytes.NewReader(buf)
					return io.NopCloser(r), nil
				}
			case *bytes.Reader:
				r.ContentLength = int64(v.Len())
				snapshot := *v
				r.GetBody = func() (io.ReadCloser, error) {
					r := snapshot
					return io.NopCloser(&r), nil
				}
			case *strings.Reader:
				r.ContentLength = int64(v.Len())
				snapshot := *v
				r.GetBody = func() (io.ReadCloser, error) {
					r := snapshot
					return io.NopCloser(&r), nil
				}
			default:
				// This is where we'd set it to -1 (at least
				// if body != NoBody) to mean unknown, but
				// that broke people during the Go 1.8 testing
				// period. People depend on it being 0 I
				// guess. Maybe retry later. See Issue 18117.
				r.ContentLength = 0
				r.GetBody = nil
			}
			// For client requests, Request.ContentLength of 0
			// means either actually 0, or unknown. The only way
			// to explicitly say that the ContentLength is zero is
			// to set the Body to nil. But turns out too much code
			// depends on NewRequest returning a non-nil Body,
			// so we use a well-known ReadCloser variable instead
			// and have the http package also treat that sentinel
			// variable to mean explicitly zero.
			if r.GetBody != nil && r.ContentLength == 0 {
				r.Body = http.NoBody
				r.GetBody = func() (io.ReadCloser, error) { return http.NoBody, nil }
			}
			if r.ContentLength != 0 {
				r.Header.Add("Content-Type", contentType)
			}
		}
		return nil
	}
}
