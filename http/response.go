package http

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"
	"net/http"
	"strings"
)

type Response struct {
	*http.Response
}

func (r *Response) Unmarshal(dest any, as ...ContentType) error {
	if len(as) > 0 {
		if !as[0].IsValid() {
			return fmt.Errorf("response: unknown content type: %s", as[0])
		}
	}
	if len(as) == 0 {
		as = []ContentType{ContentType(strings.Split(r.Header.Get("Content-Type"), ";")[0])}
	}
	defer func(Body io.ReadCloser) { _ = Body.Close() }(r.Body)
	body, err := io.ReadAll(r.Body)
	if err != nil {
		return err
	}
	switch as[0] {
	case ContentTypeXml:
		return xml.Unmarshal(body, dest)
	case ContentTypeJson:
		// Json is a common standard for API so let's use it as default
		fallthrough
	default:
		return json.Unmarshal(body, dest)
	}
}
