package http

import (
	"context"
	"fmt"
	"net/http"
	"regexp"
	"strings"
	"time"

	"gitlab.com/gorib/pry"
	"gitlab.com/gorib/session"
)

type ClientOption func(c *Client) error

func WithThreads(threads uint) ClientOption {
	return func(c *Client) error {
		c.concurrent = make(chan bool, threads)
		return nil
	}
}

func WithHttpClient(conn HttpClient) ClientOption {
	return func(c *Client) error {
		c.client = conn
		return nil
	}
}

func WithTimeout(timeout time.Duration) ClientOption {
	return func(c *Client) error {
		if conn, ok := c.client.(*http.Client); ok {
			conn.Timeout = timeout
			return nil
		} else if conn, ok := c.client.(HasTimeout); ok {
			conn.SetTimeout(timeout)
			return nil
		} else {
			return fmt.Errorf("timeout for this client cannot be set")
		}
	}
}

func WithLogger(logger pry.Logger) ClientOption {
	return func(c *Client) error {
		c.logger = logger
		return nil
	}
}

func WithDefaultRequestOptions(options ...RequestOption) ClientOption {
	return func(c *Client) error {
		c.requestOptions = options
		return nil
	}
}

type HttpClient interface {
	Do(req *http.Request) (*http.Response, error)
}

type HasTimeout interface {
	SetTimeout(timeout time.Duration)
}

func New(baseUrl string, options ...ClientOption) (*Client, error) {
	if re, err := regexp.Compile(`^https?://[^/]+`); err != nil {
		return nil, fmt.Errorf("create client: %v", err)
	} else if !re.MatchString(baseUrl) {
		return nil, fmt.Errorf(`client supports only http and https schemes, tries "%s"`, baseUrl)
	}
	c := &Client{
		baseUrl: baseUrl,
		client:  &http.Client{},
	}
	for _, opt := range options {
		if err := opt(c); err != nil {
			return nil, fmt.Errorf("client options: %v", err)
		}
	}

	return c, nil
}

type Client struct {
	client         HttpClient
	baseUrl        string
	concurrent     chan bool
	requestOptions []RequestOption
	logger         pry.Logger
}

func (c *Client) Get(ctx context.Context, uri string, opts ...RequestOption) (*Response, error) {
	return c.Do(ctx, http.MethodGet, uri, opts...)
}

func (c *Client) Post(ctx context.Context, uri string, opts ...RequestOption) (*Response, error) {
	return c.Do(ctx, http.MethodPost, uri, opts...)
}

func (c *Client) Patch(ctx context.Context, uri string, opts ...RequestOption) (*Response, error) {
	return c.Do(ctx, http.MethodPatch, uri, opts...)
}

func (c *Client) Delete(ctx context.Context, uri string, opts ...RequestOption) (*Response, error) {
	return c.Do(ctx, http.MethodDelete, uri, opts...)
}

func (c *Client) Head(ctx context.Context, uri string, opts ...RequestOption) (*Response, error) {
	return c.Do(ctx, http.MethodHead, uri, opts...)
}

func (c *Client) Options(ctx context.Context, uri string, opts ...RequestOption) (*Response, error) {
	return c.Do(ctx, http.MethodOptions, uri, opts...)
}

func (c *Client) Do(ctx context.Context, method string, uri string, opts ...RequestOption) (*Response, error) {
	if c.concurrent != nil {
		c.concurrent <- true
		defer func() { <-c.concurrent }()
	}
	return c.DoWithoutConcurrent(ctx, method, uri, opts...)
}

func (c *Client) DoWithoutConcurrent(ctx context.Context, method string, uri string, opts ...RequestOption) (*Response, error) {
	uri = c.absPath(uri)
	req, err := http.NewRequestWithContext(ctx, method, uri, nil)
	if err != nil {
		return nil, fmt.Errorf("request: %v", err)
	}
	if s := session.FromContext(ctx); s != nil {
		if s.CorrelationId != "" {
			req.Header.Set(session.HttpCorrelationIdHeader, s.CorrelationId)
		}
		if s.UserAgent != "" {
			req.Header.Set(session.HttpUserAgentHeader, s.UserAgent)
		}
		if s.UserLanguage != "" {
			req.Header.Set(session.HttpUserLanguageHeader, s.UserLanguage)
		}
		if s.SourceIp != "" {
			req.Header.Set(session.HttpSourceIpHeader, s.SourceIp)
		}
	}
	for _, opt := range c.requestOptions {
		if err := opt(req); err != nil {
			return nil, fmt.Errorf("default request option: %v", err)
		}
	}
	for _, opt := range opts {
		if err := opt(req); err != nil {
			return nil, fmt.Errorf("request option: %v", err)
		}
	}

	data, err := Body(&req.Body)
	if err != nil {
		return nil, err
	}
	if c.logger != nil {
		if len(data) > 0 {
			c.logger.Trace(fmt.Sprintf("%v %s: headers: %v data: %v", req.Method, c.absPath(uri), req.Header, string(data)))
		} else {
			c.logger.Trace(fmt.Sprintf("%v %s: headers: %v", req.Method, c.absPath(uri), req.Header))
		}
	}

	resp, err := c.client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("perform: %v", err)
	}

	if resp.StatusCode >= 300 {
		return nil, Exception(resp.Status, resp)
	}

	cType := resp.Header.Get("Content-Type")
	if c.logger != nil && strings.Index(cType, "image/") != 0 && cType != "application/pdf" {
		content, err := Body(&resp.Body)
		if err != nil {
			return nil, err
		}
		c.logger.Trace(string(content))
	}
	return &Response{resp}, nil
}

func (c *Client) absPath(url string) string {
	match := regexp.MustCompile("^(https?://[^/]+).*")
	if match.MatchString(url) {
		return url
	}
	baseUrl := c.baseUrl
	if url[0] == '/' {
		baseUrl = match.ReplaceAllString(baseUrl, "$1")
	}

	return fmt.Sprintf("%s/%s", strings.TrimRight((baseUrl), "/"), strings.TrimLeft(url, "/"))
}
