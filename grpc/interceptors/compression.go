package interceptors

import (
	"context"

	"google.golang.org/grpc"
	"google.golang.org/grpc/encoding/gzip"
)

func Compress(ctx context.Context, method string, req, reply any, cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption) error {
	opts = append(opts, grpc.UseCompressor(gzip.Name))
	return invoker(ctx, method, req, reply, cc, opts...)
}
