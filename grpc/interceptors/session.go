package interceptors

import (
	"context"

	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"

	"gitlab.com/gorib/session"
)

func Session(ctx context.Context, method string, req, reply any, cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption) error {
	if s := session.FromContext(ctx); s != nil {
		var headers []string
		if s.CorrelationId != "" {
			headers = append(headers, session.GrpcCorrelationIdHeader, s.CorrelationId)
		}
		if s.UserAgent != "" {
			headers = append(headers, session.GrpcUserAgent, s.UserAgent)
		}
		if s.UserLanguage != "" {
			headers = append(headers, session.GrpcUserLanguage, s.UserLanguage)
		}
		if s.SourceIp != "" {
			headers = append(headers, session.GrpcSourceIp, s.SourceIp)
		}
		if len(headers) > 0 {
			ctx = metadata.AppendToOutgoingContext(ctx, headers...)
		}
	}
	return invoker(ctx, method, req, reply, cc, opts...)
}
