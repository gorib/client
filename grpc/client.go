package grpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"gitlab.com/gorib/client/grpc/interceptors"
)

func New(host, port string, options ...grpc.DialOption) (*grpc.ClientConn, error) {
	options = append([]grpc.DialOption{grpc.WithChainUnaryInterceptor(interceptors.Session), grpc.WithChainUnaryInterceptor(interceptors.Compress), grpc.WithTransportCredentials(insecure.NewCredentials())}, options...)
	return grpc.NewClient(host+":"+port, options...)
}
